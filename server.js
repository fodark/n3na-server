const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
let movies = require('./movies.json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

const generateId = () => {
  const letters = '0123456789abcdefghijklmnopqrstuvwxyz';
  let r = '';
  for (let i = 0; i < 16; i++) {
    r += letters[Math.floor(Math.random() * 36)];
  }
  return r;
}

app.get('/', (_, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
})

app.get('/api', (_, res) => {
  res.json('Hello from API endpoint');
});

app.get('/api/movies', (_, res) => {
  res.json(movies);
});

app.get('/api/movies/:id', (req, res) => {
  const id = req.params.id;
  const r = movies.find((e) => e.id === id);
  res.json(r);
});

app.put('/api/movies/:id', (req, res) => {
  let movie = req.body;
  const id = req.params.id;
  movie['id'] = id;
  const idx = movies.map(e => e.id).indexOf(id);
  movies[idx] = movie;
  res.sendStatus(204);
});

app.patch('/api/movies/:id', (req, res) => {
  const year = Number(req.body.year);
  const id = req.params.id;
  const idx = movies.map(e => e.id).indexOf(id);
  let movie = movies[idx];
  movie['year'] = year;
  movies[idx] = movie;
  res.sendStatus(204);
});

app.delete('/api/movies/:id', (req, res) => {
  const id = req.params.id;
  movies = movies.filter((e) => e.id !== id);
  res.sendStatus(204);
});

app.post('/api/movies', (req, res) => {
  let movie = req.body;
  const id = generateId();
  movie['id'] = id;
  movies.push(movie);
  res.status(201).json({"id": id});
})

app.listen(process.env.PORT || 3000, () => {
  console.log(`Server listening`);
});